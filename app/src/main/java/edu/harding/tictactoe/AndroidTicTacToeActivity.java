/*
 * Copyright (C) 2010 By Frank McCown at Harding University
 * 
 * This is the solution to Tutorial 5.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.harding.tictactoe;

import edu.harding.tictactoe.R;
import edu.harding.tictactoe.TicTacToeGame.DifficultyLevel;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;
import android.widget.Toast;


public class AndroidTicTacToeActivity extends Activity {
	
	// Identify the dialog boxes
	static final int DIALOG_DIFFICULTY_ID = 0;
	static final int DIALOG_QUIT_ID = 1;
	static final int DIALOG_ABOUT = 2;
	
	// Represents the game board
	private BoardView mBoardView;
	
	// Indicates if game is currently over or not
	private boolean mGameOver = false;
	
	MediaPlayer mHumanMediaPlayer;
	MediaPlayer mComputerMediaPlayer;
	
	// Whose turn to go first
	private char mGoFirst = TicTacToeGame.HUMAN_PLAYER;
	
	// Whose turn is it
	private char mTurn = TicTacToeGame.COMPUTER_PLAYER;    
	
	private int mHumanWins = 0;
	private int mComputerWins = 0;
	private int mTies = 0;
	
	// Represents the internal state of the game
	private TicTacToeGame mGame;
	
	private TextView mInfoTextView; 
	private TextView mHumanScoreTextView;
	private TextView mComputerScoreTextView;
	private TextView mTieScoreTextView;
	
	private SharedPreferences mPrefs;
	
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
                        
        mGame = new TicTacToeGame();
        mBoardView = (BoardView) findViewById(R.id.board);
        mBoardView.setGame(mGame);
        
        // Listen for touches on the board
        mBoardView.setOnTouchListener(mTouchListener);
        
        mInfoTextView = (TextView) findViewById(R.id.information); 
        mHumanScoreTextView = (TextView) findViewById(R.id.player_score);
        mComputerScoreTextView = (TextView) findViewById(R.id.computer_score);
        mTieScoreTextView = (TextView) findViewById(R.id.tie_score);
                
        mPrefs = getSharedPreferences("ttt_prefs", MODE_PRIVATE);
        
        // Restore the scores from the persistent preference data source
        mHumanWins = mPrefs.getInt("mHumanWins", 0);  // 0 is default if pref doesn't exist
        mComputerWins = mPrefs.getInt("mComputerWins", 0);
        mTies = mPrefs.getInt("mTies", 0);      
        
        // EXTRA CHALLENGE!
        int difficultyLevel = mPrefs.getInt("difficultyLevel", 
        		TicTacToeGame.DifficultyLevel.Easy.ordinal());
        mGame.setDifficultyLevel((TicTacToeGame.DifficultyLevel.values()[difficultyLevel]));
                
        if (savedInstanceState == null) {        	
        	startNewGame();
        }
        else {        	
        	// Restore the game's state
        	// The same thing can be accomplished with onRestoreInstanceState
        	mGame.setBoardState(savedInstanceState.getCharArray("board"));
        	mGameOver = savedInstanceState.getBoolean("mGameOver");        	
        	mInfoTextView.setText(savedInstanceState.getCharSequence("info"));        	
        	mTurn = savedInstanceState.getChar("mTurn");
        	mGoFirst = savedInstanceState.getChar("mGoFirst");
        	        	
        	// EXTRA CHALLENGE
        	// If it's the computer's turn, the previous turn did not take, so go again  
        	if (!mGameOver && mTurn == TicTacToeGame.COMPUTER_PLAYER) {        		
        		int move = mGame.getComputerMove();
        		setMove(TicTacToeGame.COMPUTER_PLAYER, move);
        	}        	
        }       
        
        displayScores();
    }   
    
    @Override
	protected void onResume() {		
		super.onResume();
		
		System.out.println("onResume");
		
		mHumanMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.sword);
        mComputerMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.swish);   
	}
    
	@Override
    protected void onPause() {
        super.onPause();
                
        mHumanMediaPlayer.release();
        mComputerMediaPlayer.release();        
    }
	
	@Override
    protected void onStop() {
       super.onStop();
              
       // Save the current score, but not the state of the current game        
       SharedPreferences.Editor ed = mPrefs.edit();
       ed.putInt("mHumanWins", mHumanWins);
       ed.putInt("mComputerWins", mComputerWins);
       ed.putInt("mTies", mTies);
       
       // EXTRA CHALLENGE!
       ed.putInt("difficultyLevel", mGame.getDifficultyLevel().ordinal());
       
       ed.commit(); 
	}
	
	/*
    @Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		
		mGame.setBoardState(savedInstanceState.getCharArray("board"));
    	mGameOver = savedInstanceState.getBoolean("mGameOver");        	
    	mInfoTextView.setText(savedInstanceState.getCharSequence("info"));
    	mHumanWins = savedInstanceState.getInt("mHumanWins");
    	mComputerWins = savedInstanceState.getInt("mComputerWins");
    	mTies = savedInstanceState.getInt("mTies");
    	mTurn = savedInstanceState.getChar("mTurn");
        mGoFirst = savedInstanceState.getChar("mGoFirst");
	}*/
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {		
		super.onSaveInstanceState(outState);
		
		outState.putCharArray("board", mGame.getBoardState());		
		outState.putBoolean("mGameOver", mGameOver);
		outState.putCharSequence("info", mInfoTextView.getText());
		outState.putChar("mGoFirst", mGoFirst);
		outState.putChar("mTurn", mTurn);		
	}
    
    @Override 
    public boolean onCreateOptionsMenu(Menu menu) { 
         super.onCreateOptionsMenu(menu);         	    
         MenuInflater inflater = getMenuInflater();
         inflater.inflate(R.menu.options_menu, menu);
         return true;
    } 
    
    @Override
    protected Dialog onCreateDialog(int id) {
    	Dialog dialog = null;
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	    	
        switch(id) {
        case DIALOG_DIFFICULTY_ID:
            // Create the Difficulty dialog
        	        	
        	final CharSequence[] levels = {
        			getResources().getString(R.string.difficulty_easy),
        			getResources().getString(R.string.difficulty_harder), 
        			getResources().getString(R.string.difficulty_expert)};

        	builder.setTitle(R.string.difficulty_choose);
        	
        	// Determine which radio button should be selected
        	int selected = mGame.getDifficultyLevel().ordinal();
        	
        	builder.setSingleChoiceItems(levels, selected, new DialogInterface.OnClickListener() {
        	    public void onClick(DialogInterface dialog, int item) {
        	    	dialog.dismiss();   // Close dialog
        	    	
        	    	switch (item) {
        	    	case 0:
        	    		mGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Easy);
        	    		break;
        	    	case 1:
        	    		mGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Harder);
        	    		break;
        	    	case 2:
        	    		mGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Expert);
        	    		break;
        	    	}        	    	
        	    	
        	    	Toast.makeText(getApplicationContext(), levels[item], Toast.LENGTH_SHORT).show();
        	    }
        	});
        	dialog = builder.create();       	
            break;
        case DIALOG_QUIT_ID:
            // Create the quit confirmation dialog
        	
        	builder.setMessage(R.string.quit_question)
 	       		.setCancelable(false)
 	       		.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
 	       			public void onClick(DialogInterface dialog, int id) {
 	       				AndroidTicTacToeActivity.this.finish();
 	       			}
 	       		})
 	       		.setNegativeButton(R.string.no, null);
        	dialog = builder.create();
        	
            break;
        case DIALOG_ABOUT:
        	Context context = getApplicationContext();
        	LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        	View layout = inflater.inflate(R.layout.about_dialog, null); 		

			builder.setView(layout);
			builder.setPositiveButton("OK", null);	
			dialog = builder.create();   
        	break;
        }
        
        return dialog;        
    }
      
    
    // Handles menu item selections 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.new_game:
        	startNewGame();
            return true;
        case R.id.ai_difficulty: 
        	showDialog(DIALOG_DIFFICULTY_ID);        	
        	return true;
        case R.id.reset_scores:
        	mHumanWins = 0;
        	mComputerWins = 0;
            mTies = 0;
            displayScores();
            return true;
        case R.id.about:
        	showDialog(DIALOG_ABOUT);
        	return true;
       
        }
        return false;
    }
    
    // Show the scores
    private void displayScores() {
    	mHumanScoreTextView.setText(Integer.toString(mHumanWins));
    	mComputerScoreTextView.setText(Integer.toString(mComputerWins));
    	mTieScoreTextView.setText(Integer.toString(mTies));
    }
    
    // Set up the game board. 
    private void startNewGame() {   	
    	
    	mGame.clearBoard();    	
    	mBoardView.invalidate();   // Redraw the board    	

    	// Alternate who goes first
    	if (mGoFirst == TicTacToeGame.COMPUTER_PLAYER) {    		
    		mGoFirst = TicTacToeGame.HUMAN_PLAYER;
    		mTurn = TicTacToeGame.COMPUTER_PLAYER;
    		mInfoTextView.setText(R.string.first_computer);
    		int move = mGame.getComputerMove();
    		setMove(TicTacToeGame.COMPUTER_PLAYER, move);
    	}
    	else {
    		mGoFirst = TicTacToeGame.COMPUTER_PLAYER;
    		mTurn = TicTacToeGame.HUMAN_PLAYER;
    		mInfoTextView.setText(R.string.first_human); 
    	}	
    	
    	mGameOver = false;    	
    } 
    
    // Make a move
    private boolean setMove(char player, int location) {
    	
    	if (player == TicTacToeGame.COMPUTER_PLAYER) {    		
    		// Make the computer move after a delay of 1 second
    		final int loc = location;
	    	Handler handler = new Handler();     		
    		handler.postDelayed(new Runnable() {
                public void run() {
                	mGame.setMove(TicTacToeGame.COMPUTER_PLAYER, loc);
                	mBoardView.invalidate();   // Redraw the board
                	
                	// EXTRA CHALLENGE!
                	// Exception thrown if orientation is changed before making computer move
                	try {
                		mComputerMediaPlayer.start();
                	}
                	catch (IllegalStateException e) {};  
                	
                	int winner = mGame.checkForWinner();
                	if (winner == 0) {
                		mTurn = TicTacToeGame.HUMAN_PLAYER;	                                	
                		mInfoTextView.setText(R.string.turn_human);
                	}
                	else 
    	            	endGame(winner);                              	
                } 
     		}, 1000);     		
                
    		return true;
    	}
    	else if (mGame.setMove(TicTacToeGame.HUMAN_PLAYER, location)) { 
    		mTurn = TicTacToeGame.COMPUTER_PLAYER;
        	mBoardView.invalidate();   // Redraw the board
    	   	mHumanMediaPlayer.start();    	   	
    	   	return true;
    	}
    		   	    	
    	return false;
    }
    
    // Game is over logic
    private void endGame(int winner) {
    	if (winner == 1) {
    		mTies++;
    		mTieScoreTextView.setText(Integer.toString(mTies));
    		mInfoTextView.setText(R.string.result_tie); 
    	}
    	else if (winner == 2) {
    		mHumanWins++;
    		mHumanScoreTextView.setText(Integer.toString(mHumanWins));
    		mInfoTextView.setText(R.string.result_human_wins);
    	}
    	else if (winner == 3) {
    		mComputerWins++;
    		mComputerScoreTextView.setText(Integer.toString(mComputerWins));
    		mInfoTextView.setText(R.string.result_computer_wins);
    	}
    	
    	mGameOver = true;
    }
    
    // Listen for touches on the board
    private OnTouchListener mTouchListener = new OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
       	        	
        	// Determine which cell was touched	    	
	    	int col = (int) event.getX() / mBoardView.getBoardCellWidth();
	    	int row = (int) event.getY() / mBoardView.getBoardCellHeight();
	    	int pos = row * 3 + col;
	    		    	
	    	if (!mGameOver && mTurn == TicTacToeGame.HUMAN_PLAYER &&
	    			setMove(TicTacToeGame.HUMAN_PLAYER, pos)) {        		
            	
            	// If no winner yet, let the computer make a move
            	int winner = mGame.checkForWinner();
            	if (winner == 0) { 
            		mInfoTextView.setText(R.string.turn_computer); 
            		int move = mGame.getComputerMove();
            		setMove(TicTacToeGame.COMPUTER_PLAYER, move);            		
            	} 
            	else
            		endGame(winner);            	
            }
	    	
	    	// So we aren't notified of continued events when finger is moved
	    	return false;   
        } 
    };
}